import './App.css';
import Rating from '@material-ui/lab/Rating';
import { useEffect, useState } from 'react';
import { MuiThemeProvider, responsiveFontSizes, Typography, CircularProgress } from '@material-ui/core';
import QuoteService from './services/QuoteService';
import StorageService from './services/StorageService';
import Quote from 'models/Quote';
import theme from 'theme/theme';

const stringSimilarity = require('string-similarity');
const sw = require('stopword');

const App = () => {
  const [quote, setQuote] = useState(null);
  const [rating, setRating] = useState(null);
  const [errorState, setErrorState] = useState(false);

  useEffect(() => {
    let listAllQuotes = null;

    QuoteService.getQuotes()
      .then(response => {
        if (response.status === 200 && response.data)
          listAllQuotes = response.data;
      })
      .catch(() => {
        setErrorState(true);
      })
      .finally(() => {
        listAllQuotes != null && StorageService.saveAllQuotes(listAllQuotes)
      });

    displayRandomQuote();
  }, []);

  const findSimilarQuote = (currQuote) => {
    let fullList = StorageService.getAllQuotes();

    // Remove current quote from list
    let listAllQuotes = fullList
      .filter(el => {
        return currQuote.text === el.en ? false : true
      })

    // Update stored list with removed quote
    StorageService.saveAllQuotes(listAllQuotes);

    // Get quote strings from list; remove stopwords
    let removedStopWordsList = listAllQuotes.map(el => {
      let oldString = el.en.split(' ');
      let newString = sw.removeStopwords(oldString, sw.en);
      return newString.join(' ');
    });

    // Use stringSimilarity to find the best match
    let bestMatch = fullList[
      stringSimilarity.findBestMatch(currQuote.text, removedStopWordsList).bestMatchIndex
    ];

    setQuote(new Quote(bestMatch.id, bestMatch.en, bestMatch.author));
  }

  const displayRandomQuote = () => {
    setRating(null);
    QuoteService.getQuotes(true)
      .then(response => {
        if (response.status === 200 && response.data)
          setQuote(new Quote(response.data.id, response.data.en, response.data.author));
      });
  }

  const handleQuoteRated = (handledQuote, newRating) => {
    QuoteService.rateQuote(handledQuote.id, newRating);

    newRating >= 4 ?
      findSimilarQuote(handledQuote)
      :
      displayRandomQuote();
  }

  return (
    <MuiThemeProvider theme={responsiveFontSizes(theme)}>
      <div className="app">
        {errorState ?
          <Typography variant="h4">
            Error communicating to API<br />
            Please try again later.
          </Typography>
          :
          <>
            {quote ?
              <div className="quote-container">
                <div className="quote-text">
                  <Typography variant="h4">
                    {quote.text}
                  </Typography>
                  <Typography variant="h5" className="quote-author">
                    - {quote.author}
                  </Typography>
                  <Rating
                    name="quote-rating"
                    size="large"
                    value={rating}
                    onChange={(event, newRating) => {
                      console.log(rating, newRating);
                      handleQuoteRated(quote, newRating);
                    }}
                  />
                </div>
              </div>
              :
              <CircularProgress />
            }
          </>
        }
      </div>
    </MuiThemeProvider>
  );
}


export default App;
