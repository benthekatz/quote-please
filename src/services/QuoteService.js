import axios from 'axios';
import Constants from 'config/Constants';

class QuoteService {
  static getQuotes(random = false) {
    return random ?
      axios.get(`${Constants.BASE_URL}/quotes/random/lang/en`)
      :
      axios.get(`${Constants.BASE_URL}/quotes/lang/en`);
  }

  static rateQuote(quoteId, newRating) {
    return axios.post(
      `${Constants.BASE_URL}/quotes/vote`,
      {
        "quoteId": quoteId,
        "newVote": newRating
      }
    );
  }
}

export default QuoteService;
