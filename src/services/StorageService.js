class StorageService {
  static saveAllQuotes(quotesArray) {
    localStorage.setItem("quotes", JSON.stringify(quotesArray));
  }

  static getAllQuotes() {
    return JSON.parse(localStorage.getItem("quotes"));
  }
}

export default StorageService;
